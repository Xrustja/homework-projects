import React from 'react';
import './App.css';
import ProductList from "./components/ProductList";
import Modal from "./components/Modal";
import Button from "./components/Button";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            productNameInCart: '',
            favorite: [],
            cart: []
        }
        ;


        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    getProducts() {
        fetch('products.json')
            .then(response => response.json())
            .then(data => this.setState({products: data})
            )
    }

    openModal(e) {
        this.setState({productNameInCart: e.target.name})
    }

    closeModal() {
        this.setState({productNameInCart: ''})
    }

    componentDidMount() {
        this.getProducts();

    }

    render() {
        return (
            <div className="App">
                <ProductList data={this.state.products} onClick={this.openModal}/>

                <Modal
                    closeButton={true}
                    text={`${this.state.productNameInCart} добавлен в корзину`}
                    onClose={this.closeModal}
                    show={this.state.productNameInCart !== ''}
                    actions={[
                        <Button
                            className='btn-modal'
                            text='Оформить заказ'
                            onClick={() => alert('Отличный выбор!')}
                        />
                    ]}
                />
            </div>

        )
    }

}

export default App;





