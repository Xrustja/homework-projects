import React from 'react'
import './ProductCard.scss'
import Button from '../Button'
import PropTypes from 'prop-types'

class ProductCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            favorite: false,
            card: false
        };
        this.addToFavorite = this.addToFavorite.bind(this);
        this.addToCard = this.addToCard.bind(this);

    }

    addToFavorite() {
        if (!this.state.favorite) {
            this.setState({favorite: true});
            const favoriteInLS = JSON.parse(localStorage.getItem('favorite')) || [];
            favoriteInLS.push(this.props.id);
            localStorage.setItem('favorite', JSON.stringify(favoriteInLS));

        } else {
            const favoriteInLS = JSON.parse(localStorage.getItem('favorite')).filter(id => id !== this.props.id);
            localStorage.setItem('favorite', JSON.stringify(favoriteInLS));
            this.setState({favorite: false});

        }
    }

    addToCard(e) {
        if (!this.state.card) {
            this.setState({card: true});
            if (localStorage.getItem('card') === null) {
                const cardInLS = [];
                cardInLS.push(this.props.id);
                localStorage.setItem('card', JSON.stringify(cardInLS))
            } else {
                const cardInLS = JSON.parse(localStorage.getItem('card'));
                cardInLS.push(this.props.id);
                localStorage.setItem('card', JSON.stringify(cardInLS));
            }
        }
        this.props.onClick(e);
    }

    componentDidMount() {
        if (localStorage.getItem('favorite') !== null) {
            const favoriteInLS = JSON.parse(localStorage.getItem('favorite'));
            if (favoriteInLS.includes(this.props.id)) {
                this.setState({favorite: true})
            }
        }
        if (localStorage.getItem('card') !== null) {
            const favoriteInLS = JSON.parse(localStorage.getItem('card'));
            if (favoriteInLS.includes(this.props.id)) {
                this.setState({card: true})
            }
        }
    }

    render() {
        const liked = this.state.favorite ? <i className="fas fa-heart"/> : <i className="far fa-heart"/>;
        return (
            <div className='card-container'>
                <img className='card-img' src={this.props.imgUrl} alt="Temporarily missing"/>
               <div className='card-text_container'>
                <h4 className='card-name'>{this.props.name}</h4>
                <h4 className='card-id'>Код товара: {this.props.id}</h4>
               </div>
                <div className='card-footer'>
                    <p className='card-price'>{this.props.price} ₴</p>
                    <Button
                        className='btn-favorite'
                        text={liked}
                        onClick={this.addToFavorite}

                    />
                    <Button
                        productName={this.props.name}
                        className='card-button'
                        text='Купить'
                        onClick={this.addToCard}/>
                </div>
            </div>
        )
    }
}

ProductCard.propTypes = {
    price:PropTypes.string,
    id:PropTypes.string,
    imgUrl:PropTypes.string,
    name:PropTypes.string,
};

ProductCard.defaultProps ={
    price: 'Нет в наличии'
};

export default ProductCard;