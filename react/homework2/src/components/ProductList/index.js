import React from 'react';
import ProductCard from '../ProductCard'
import './ProductList.scss'
import PropTypes from "prop-types";

class ProductList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        const products = this.props.data;

        return (
            <div className="cards-container">
                {
                    products.map(product => <ProductCard
                        onClick={this.props.onClick}
                        name={product.name}
                        imgUrl={product.imgUrl}
                        price={product.price}
                        id={product.id}
                        key={product.id}
                    />)
                }
            </div>
        );
    }
}

ProductList.propTypes = {
    onClick: PropTypes.func,
    data: PropTypes.array,

};

export default ProductList;
