import React from "react";
import './Button.scss';
import classnames from 'classnames';
import PropTypes from 'prop-types'

class Button extends React.Component {
    constructor(props) {
        super(props);
console.log(props, 'IN BTN');
        this.onBtnClick = this.onBtnClick.bind(this);
    }

    onBtnClick(e) {
        this.props.onClick(e);

    }

    render() {

        return (
            <button className={classnames('btn', this.props.className)}
                    style={{backgroundColor: this.props.backgroundColor}}
                    name = {this.props.productName}
                    id = {this.props.id}
                    onClick={this.onBtnClick}>{this.props.text}
            </button>
        )
    }
}

Button.propTypes = {
    onClick: PropTypes.func,
    className: PropTypes.string,
    backgroundColor: PropTypes.string,
    text: PropTypes.string
};

Button.defaultProps ={
    backgroundColor: '#B1EC87'
};


export default Button;
