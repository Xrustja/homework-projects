import React from 'react';
import './App.css';
import Button from "./components/Button";
import Modal from "./components/Modal";


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clickedModalId: '',
        };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal(id) {
        this.setState({clickedModalId: id})
    }

    closeModal() {
        this.setState({clickedModalId: ''})
    }

    render() {

        return (
            <div className="App">
                <Button className='btn-main'
                        text='Open first modal'
                        backgroundColor='#ffe28a'
                        onClick={() => this.openModal('1')}>
                </Button>
                <Button className='btn-main'
                        text='Open second modal'
                        backgroundColor='#fcc0f6'
                        onClick={() => this.openModal('2')}>
                </Button>
                <Modal
                    closeButton={true}
                    text={'Once you delete this file, it won\'t be possible to undo this action.\nAre you sure you want to delete it?'}
                    onClose={this.closeModal}
                    show={this.state.clickedModalId === '1'}
                    actions={[
                        <Button
                            className='btn-modal'
                            text='Ok'
                            onClick={() => alert('You are risky guy =)')}
                        />,
                        <Button
                            className='btn-modal'
                            text='Cancel'
                            onClick={() => alert('Great choice!')}
                        />
                    ]}
                />

                <Modal
                    closeButton={false}
                    text={'You win a lottery!!! Call us to get your prise!!'}
                    onClose={this.closeModal}
                    title='Lucky day!!!'
                    show={this.state.clickedModalId === '2'}
                    actions={[
                        <Button
                            className='btn-modal'
                            text='Show number'
                            onClick={() => alert('+480932736482')}
                        />
                    ]}
                />
            </div>
        )
    }
}

export default App;


//
//
