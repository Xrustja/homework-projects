import React from 'react';
import './Button.scss'
import classnames from 'classnames'
import PropTypes from 'prop-types'

class Button extends React.Component {
    constructor(props) {
        super(props);

        this.onBtnClick = this.onBtnClick.bind(this);
    }

    onBtnClick(e) {
        this.props.onClick(e);
    }

    render() {

        return (
            <button className={classnames('btn', this.props.className)}
                    style={{backgroundColor: this.props.backgroundColor}}
                    onClick={this.onBtnClick}>{this.props.text}
            </button>
        )
    }
}

Button.propTypes = {
    onClick: PropTypes.func,
    className: PropTypes.string,
    backgroundColor: PropTypes.string,
    text: PropTypes.string
};

export default Button;
