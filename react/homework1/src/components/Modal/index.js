import React from 'react'
import './Modal.scss'
import Button from "../Button";
import PropTypes from "prop-types";

class Modal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            className: 'hidden'
        };

        this.closeModal = this.closeModal.bind(this);
    }

    closeModal(e) {
        if (e.target === e.currentTarget) {
            this.props.onClose();
        }
    }

    render() {
        return (
            <div className={'modal ' + (this.props.show ? "" : this.state.className)} onClick={this.closeModal}>
                <div className='modal-wrapper'>
                    <div className="modal-header">
                        <h5 className="modal-title">{this.props.title || 'Do you want to delete this file?'}</h5>
                        {this.props.closeButton
                            ? <Button className='btn-close' onClick={this.closeModal} text='&#10006;'/>
                            : ''}
                    </div>
                    <div className="modal-body">
                        <p>{this.props.text}</p>
                    </div>
                    <div className="modal-footer">
                        {this.props.actions}
                    </div>
                </div>

            </div>
        )
    }
}


Modal.propTypes = {
    onClose: PropTypes.func,
    show: PropTypes.bool,
    title: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.array,


};

export default Modal;