import React from 'react';
import ProductList from '../../components/ProductList'
import {useSelector} from 'react-redux';
import Modals from "../../components/Modals";

export default function Products() {
    const products = useSelector(state => state.product.data);
    return (
        <div className="main">
            <ProductList data={products}/>
            <Modals/>
        </div>

    )
}



//
// const mapStateToProps = (state) => {
//     return {
//         products: state.product.products,
//     };
// };
//
// const mapDispatchToProps = (dispatch) => {
//     return {
//         productsDispatch: dispatch(itemsFetchData())
//     };
// };
//
// export default connect(mapStateToProps, mapDispatchToProps)(Products);
//
//






