import React from 'react'
import PropTypes from 'prop-types'
import Button from "../Button";
import './Modal.scss'

export default function Modal(props) {


    function closeModal(e) {
        if (e.target === e.currentTarget) {
            props.onClose();
        }
    }

    return (
        <div className={'modal ' + (props.show ? "" : 'hidden')} onClick={closeModal}>
            <div className='modal-wrapper'>
                <div className="modal-header">
                    <h5 className="modal-title">{props.title}</h5>
                    {props.closeButton
                        ? <Button className='btn-close' onClick={closeModal} text='&#10006;'/>
                        : ''}
                </div>
                <div className="modal-body">
                    <p>{props.text}</p>
                </div>
                <div className="modal-footer">
                    {props.actions}
                </div>
            </div>
        </div>
    )
}


Modal.propTypes = {
    onClose: PropTypes.func,
    show: PropTypes.bool,
    title: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.array,
};

Modal.defaultProps = {
    title: 'Спасибо что выбрали нас!'
};

