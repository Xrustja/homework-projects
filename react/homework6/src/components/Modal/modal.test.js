import React from 'react';
import {configure, shallow} from 'enzyme';
import Modal from "./index.js";
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

describe('Modal Component', () => {

    it("render correct class name", () => {
        const props = {
                show: false
            },
            testModal = shallow(<Modal {...props} />).find(
                '.modal'
            );
        expect(testModal.hasClass("modal hidden")).toEqual(true);
    });

    it("render correct text", () => {
        const props = {
                text: 'Modal test text'
            },
            testModal = shallow(<Modal {...props} />).find(
                '.modal-body p'
            );
        expect(testModal.text()).toEqual('Modal test text');
    });

    it("render correct onClose prop type", () => {
        const props = {
                show: true,
                onClose: () => {
                }
            },
            testModal = shallow(<Modal {...props} />);
        expect(typeof testModal.props().onClick).toBe('function');
    });

});
