import React from 'react';
import {configure, shallow} from 'enzyme';
import Button from "./index.js";
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

describe('Button Component', () => {

    it('render button tag', () => {
        const testButton = shallow(<Button text='Some btn name' />);
        expect(testButton.exists('button')).toBeTruthy();
    });

    it('concatenating default className and added className in props ', () => {
        const testButton = shallow(<Button text='Some btn name' className='buy-button' />);
        expect(testButton.find('button').hasClass('btn buy-button')).toBeTruthy();
    });

    it('should call props.onClick() when button is clicked', () => {
        const onClick = jest.fn();
        const testButton = shallow(<Button text='Some btn name' onClick={onClick}/>);
        testButton.find('button').simulate('click');
        expect(onClick).toHaveBeenCalledTimes(1);
    })

});