export const UPDATE_FORM_DATA = 'UPDATE_FORM_DATA';

export function updateFormData(formData) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_FORM_DATA,
            formData: formData
        })
    };
}
