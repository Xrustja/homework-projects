import {UPDATE_FORM_DATA} from '../actions/formActions'

const initialState = {
    formData: {
        firstName: '',
        lastName: '',
        age: '',
        address: '',
        phoneNumber: '+38'
    }
};


export default function formReducer(state = initialState, action) {
    switch (action.type) {
        case UPDATE_FORM_DATA:
            return {
                formData: {
                    ...state.formData,
                   ...{ [action.formData.name]: action.formData.value
                   } }
            };
        default:
            return state
    }
}
