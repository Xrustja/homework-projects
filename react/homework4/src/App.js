import React from 'react';
import './App.scss';
import Navigation from './components/Navigation'
import RouterConfig from "./components/RouterConfig";
import {BrowserRouter as Router} from "react-router-dom";
import {useDispatch} from "react-redux";
import {itemsFetchData} from "./redux/actions/productActions";
import {useEffect} from "react";

function App() {
    const productDispatch = useDispatch();
    useEffect (() => productDispatch(itemsFetchData()),[]);

    return (
        <div className="App">
            <Router>
                <Navigation/>
                <RouterConfig/>
            </Router>
        </div>

    )
}


export default App;





