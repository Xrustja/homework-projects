import {NavLink} from "react-router-dom";

export default function Navigation() {
    return (
        <nav>
            <ul className='nav-container'>
                <li><NavLink to='/' exact activeClassName='selected'> Все товары </NavLink></li>
                <li><NavLink to='/cart' exact activeClassName='selected'> Корзина </NavLink></li>
                <li><NavLink to='/favourite' exact activeClassName='selected'> Избранное </NavLink></li>
            </ul>
        </nav>
    )
}