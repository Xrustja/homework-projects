import React, {useEffect, useState} from 'react'
import PropTypes from 'prop-types'
import Button from '../Button'
import * as LocalStorageServices from "../../services/LocalStorageServices";
import './ProductCard.scss'
import {useDispatch} from "react-redux";
import {setModalStatus} from "../../redux/actions/modalActions";


export default function ProductCard(props) {
    const [cart, setCart] = useState(false);
    const [favourite, setFavourite] = useState(false);
    const modalDispatch = useDispatch();

    function addToFavourite(props) {
        let favouriteInLS = LocalStorageServices.getLSData('favourite') || [];
        if (!favourite) {
            favouriteInLS.push(props.id);
        } else {
            favouriteInLS = favouriteInLS.filter(LsId => LsId !== props.id);
            if (typeof (props.refresh) === 'function') props.refresh(favouriteInLS);
        }
        setFavourite(!favourite);
        LocalStorageServices.setLSData('favourite', favouriteInLS);
    }

    function handleClick() {
        if (!cart) {
            setCart(true);
            const cartInLS = LocalStorageServices.getLSData('cart') || [];
            cartInLS.push(props.id);
            LocalStorageServices.setLSData('cart', cartInLS);
            modalDispatch(setModalStatus({
                modalStatus: true,
                lastAction: 'add',
                prodName: props.name
            }));
        }
        else {
            modalDispatch(setModalStatus({
                modalStatus: true,
                prodName: props.name,
                remove: () => removeFromCart(props)
            }));
        }
    }

    function removeFromCart(props) {
        if (cart) {
            const cartInLS = LocalStorageServices.getLSData('cart').filter(LsId => LsId !== props.id);
            if (typeof (cartInLS) === 'object') {
                LocalStorageServices.setLSData('cart', cartInLS);
                modalDispatch(setModalStatus());
                setCart(false);
                if (typeof (props.refresh) === 'function') props.refresh();
            }
        }
    }

    useEffect(() => {
        if (LocalStorageServices.getLSData('favourite') !== null) {
            const favouriteInLS = LocalStorageServices.getLSData('favourite');
            if (favouriteInLS.includes(props.id)) {
                setFavourite(true)
            }
        }

        if (LocalStorageServices.getLSData('cart') !== null) {
            const favouriteInLS = LocalStorageServices.getLSData('cart');
            if (favouriteInLS.includes(props.id)) {
                setCart(true)
            }
        }


    }, []);


    const liked = favourite ? <i className="fas fa-heart"/> : <i className="far fa-heart"/>;
    const buyBtnText = cart ? 'Удалить с корзины' : 'Купить';

    return (
        <>

            <div className='card-container'>
                <img className='card-img' src={props.imgUrl} alt="Temporarily missing"/>
                <div className='card-text_container'>
                    <h4 className='card-name'>{props.name}</h4>
                    <h4 className='card-id'>Код товара: {props.id}</h4>
                </div>
                <div className='card-footer'>
                    <p className='card-price'>{props.price} ₴</p>
                    <Button
                        className='btn-favourite'
                        text={liked}
                        onClick={() => addToFavourite(props)}
                    />
                    <Button
                        productName={props.name}
                        className='card-button'
                        text={buyBtnText}
                        onClick={() => handleClick()}/>
                </div>
            </div>

        </>
    )

}

ProductCard.propTypes = {
    price: PropTypes.string,
    id: PropTypes.string.isRequired,
    imgUrl: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    onClick: PropTypes.func,
};

ProductCard.defaultProps = {
    price: 'Нет в наличии'
};

