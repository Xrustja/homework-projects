import React from "react";
import './Button.scss';
import classnames from 'classnames';
import PropTypes from 'prop-types'

export default function Button(props) {

    return (
        <button className={classnames('btn', props.className)}
                style={{backgroundColor: props.backgroundColor}}
                name={props.productName}
                id={props.id}
                onClick={props.onClick}>{props.text}
        </button>
    )

}

Button.propTypes = {
    onClick: PropTypes.func,
    className: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string,
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired
};

Button.defaultProps = {
    backgroundColor: '#B1EC87'
};


