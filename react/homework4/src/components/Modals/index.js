import Modal from "../Modal";
import Button from "../Button";
import {useDispatch, useSelector} from "react-redux";
import {SET_MODAL_STATUS} from "../../redux/actions/modalActions";

export default function Modals() {
    const modalData = useSelector(state => state.modal);
    const modalDispatch = useDispatch();


    function closeModal() {
        modalDispatch({type: SET_MODAL_STATUS})
    }

    return (
        <>
            {(modalData.lastAction === 'add')
                ?
                <Modal
                    closeButton={true}
                    text={`${modalData.prodName} добавлен в корзину`}
                    onClose={closeModal}
                    show={modalData.modalStatus}
                    actions={[
                        <Button
                            key='1'
                            className='btn-modal'
                            text='Оформить заказ'
                            onClick={() => alert('Отличный выбор!')}
                        />
                    ]}
                />
                :
                <Modal
                    closeButton={true}
                    title={'Подтвердите удаление'}
                    text={`Вы действительно хотите удалить ${modalData.prodName} с корзины?`}
                    onClose={closeModal}
                    show={modalData.modalStatus}
                    actions={[
                        <Button
                            key='1'
                            className='btn-modal'
                            text='Отменить'
                            onClick={closeModal}
                        />,
                        <Button
                            key='2'
                            className='btn-modal'
                            text='Удалить'
                            onClick={() => modalData.remove()}
                        />
                    ]}
                />}
        </>
    )
}