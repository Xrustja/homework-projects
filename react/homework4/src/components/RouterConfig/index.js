import React from 'react';
import {Route, Switch} from "react-router-dom";
import Products from "../../pages/Products";
import Favourite from "../../pages/Favourite";
import Cart from '../../pages/Cart'

export default function RouterConfig() {
    return (

        <Switch>
            <Route exact path='/'>
                <Products/>
            </Route>
            <Route exact path='/cart'>
                <Cart/>
            </Route>
            <Route exact path='/favourite'>
                <Favourite/>
            </Route>
        </Switch>

    )
}