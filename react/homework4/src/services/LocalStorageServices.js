export function getLSData(key) {
    return JSON.parse(localStorage.getItem(key))
}

export function setLSData(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
}

