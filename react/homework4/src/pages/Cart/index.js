import React, {useEffect, useState} from 'react';
import ProductList from "../../components/ProductList";
import * as LocalStorageServices from "../../services/LocalStorageServices";
import {useSelector} from "react-redux";
import Modals from "../../components/Modals";

export default function Cart() {
    const products = useSelector(state => state.product.data);
    const [productsInCart, setProductsInCart] = useState([]);

    function getProducts() {
        const productsInLsCart = LocalStorageServices.getLSData('cart') || [];
        setProductsInCart(productsInLsCart
            .map(id => products
            .find(product => product.id === id))
            .filter(item => item !== undefined));
    }

    useEffect(() => getProducts(), [products]);

    const pageTitle = productsInCart.length > 0 ? "Товары в вашей корзине:" : 'У вас нет товаров в корзине';
    return (
        <div>
            <h2>{pageTitle}</h2>
            <ProductList data={productsInCart} refresh={getProducts}/>
            <Modals/>
        </div>
    )
}

