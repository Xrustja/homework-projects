export const SET_MODAL_STATUS = 'SET_MODAL_STATUS';


export function setModalStatus(modalData) {
    return (dispatch) => {
        dispatch({
            type: SET_MODAL_STATUS,
            modalData,
        })
    };
}
