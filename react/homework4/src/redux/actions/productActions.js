export const GET_PRODUCTS = 'GET_PRODUCTS';


export function getProducts(dataProducts) {
    return {
        type: GET_PRODUCTS,
        dataProducts
    }

}

export function itemsFetchData() {
    return (dispatch) => {
        fetch('products.json')
            .then(response => response.json())
            .then(dataProducts => dispatch(getProducts(dataProducts))
            )
    };
}