import {SET_MODAL_STATUS} from "../actions/modalActions";

const initialState = {
    modalStatus: false,
};

export default function modalReducer(state = initialState, action) {
    switch (action.type) {
        case SET_MODAL_STATUS:
            return {
                ...action.modalData
            };

        default:
            return state
    }
}
