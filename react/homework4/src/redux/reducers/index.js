import productsReducer from "./productsReducer";
import {combineReducers} from "redux";
import modalReducer from "./modalReducer";

export const rootReducer = combineReducers({
    product: productsReducer,
    modal: modalReducer
});