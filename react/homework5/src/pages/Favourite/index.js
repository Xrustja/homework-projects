import React, {useEffect, useState} from 'react';
import ProductList from "../../components/ProductList";
import * as LocalStorageServices from "../../services/LocalStorageServices";
import {useSelector} from 'react-redux';
import Modals from "../../components/Modals";

export default function Favorite() {
    const products = useSelector(state => state.product.data);
    const [productsInFavourite, setProductsInFavourite] = useState([]);

    function getProducts(favouriteInLS) {
        const productsInLsFavourite = favouriteInLS || LocalStorageServices.getLSData('favourite') || [];
        setProductsInFavourite(productsInLsFavourite
            .map(id => products
            .find(product => product.id === id))
            .filter(item => item !== undefined)
        );
    }

    useEffect(() => getProducts(), [products]);

    const pageTitle = productsInFavourite.length > 0 ? "Ваши избранные продукты:" : 'У вас нет избранных товаров';

    return (
        <div>
            <h2>{pageTitle}</h2>
            <ProductList data={productsInFavourite} refresh={getProducts}/>
            <Modals/>
        </div>
    )
}
