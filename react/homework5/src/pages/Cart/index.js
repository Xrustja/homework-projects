import React, {useEffect, useState} from 'react';
import ProductList from "../../components/ProductList";
import * as LocalStorageServices from "../../services/LocalStorageServices";
import {useSelector} from "react-redux";
import Modals from "../../components/Modals";
import Form from "../../components/Form";
import './Cart.scss'

export default function Cart() {
    const products = useSelector(state => state.product.data);
    const [productsInCart, setProductsInCart] = useState([]);

    function getProducts() {
        const productsInLsCart = LocalStorageServices.getLSData('cart') || [];
        setProductsInCart(productsInLsCart
            .map(id => products
                .find(product => product.id === id))
            .filter(item => item !== undefined));
    }

    useEffect(() => getProducts(), [products]);

    return (
        productsInCart.length > 0
            ?
    <div>
            <div className='cart-main-container'>
                <div className='form-container'>
                    <Form products={productsInCart} refresh={getProducts}/>
                </div>
                <div className='products-container'>
                    <ProductList data={productsInCart} refresh={getProducts}/>
                    <Modals/>
                </div>
            </div>
        </div>
            :  <h2> У вас нет товаров в корзине </h2>
       )
}

