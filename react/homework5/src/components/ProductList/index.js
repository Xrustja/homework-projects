import React from "react";
import ProductCard from '../ProductCard'
import './ProductList.scss'
import PropTypes from "prop-types";

export default function ProductList(props) {

    const products = props.data;


    return (
        <div className="cards-container">
            {
                products.map(product => <ProductCard
                    onClick={props.onClick}
                    refresh = {props.refresh}
                    name={product.name}
                    imgUrl={product.imgUrl}
                    price={product.price}
                    id={product.id}
                    key={product.id}
                />)
            }
        </div>
    );

}

ProductList.propTypes = {
    onClick: PropTypes.func,
    data: PropTypes.array.isRequired,
};