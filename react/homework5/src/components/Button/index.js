import React from "react";
import './Button.scss';
import classnames from 'classnames';
import PropTypes from 'prop-types'

export default function Button(props) {

    return (
        <button className={classnames('btn', props.className)}
                style={{backgroundColor: props.backgroundColor}}
                name={props.productName}
                id={props.id}
                onClick={props.onClick}
                type={props.type}
        >{props.text}

        </button>
    )

}

Button.propTypes = {
    disabled:PropTypes.bool,
    onClick: PropTypes.oneOfType(PropTypes.func ,PropTypes.bool),
    className: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string,
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired
};

Button.defaultProps = {
    type: 'button',
    disabled:false
};


