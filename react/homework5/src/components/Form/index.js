import React from "react";
import {Formik} from "formik";
import * as yup from "yup";
import './Form.scss'
import {useDispatch, useSelector} from "react-redux";
import {updateFormData} from '../../redux/actions/formActions'
import {setLSData} from '../../services/LocalStorageServices'
import Button from "../Button";

export default function Form(props) {
    const formDispatch = useDispatch();
    const formData = useSelector(state => state.form.formData);
    const validationSchema = yup.object().shape({
        firstName: yup.string().typeError('Должно быть строкой').required('Введите ваше имя'),
        lastName: yup.string().typeError('Должно быть строкой').required('Введите вашу фамилию'),
        age: yup.number().typeError('Должно быть числом').required('Введите ваш возраст').integer(),
        address: yup.string().typeError('Должно быть строкой').required('Введите ваш адрес: Город, улица, номер дома, квартира'),
        phoneNumber: yup.string()
            .required()
            .matches(/^[0-9,+]+$/, "Должно быть числом")
            .min(13, 'Введите ваш номер в формате +38...')
            .max(13, 'Номер должен содержать 14 символов')
    });


    function onElementChange(target) {
        formDispatch(updateFormData(target.target))
    }

    return (
        <div>
            <Formik
                initialValues={
                    formData
                }
                validateOnBlur
                onSubmit={(values => {
                    console.log('Данные о покупателе => ', values,);
                    console.log('Купленные товары', props.products);
                    setLSData('cart', '');
                    alert('Спасибо за покупку!!!');
                    if (typeof (props.refresh) === 'function') props.refresh();
                })}
                validationSchema={validationSchema}
            >
                {({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleBlur,
                      isValid,
                      handleSubmit,
                      dirty
                  }) =>
                    <div className='form'>
                        <p>
                            <label htmlFor="firstName">Имя </label>
                            <input className={'input'} type="text"
                                   name='firstName'
                                   onChange={(e) => {
                                       handleChange(e);
                                       onElementChange(e)
                                   }}
                                   onBlur={handleBlur}
                                   value={values.firstName}/>
                        </p>
                        {touched.firstName && errors.firstName && <p className='error'>{errors.firstName}</p>}
                        <p>
                            <label htmlFor="lastName">Фамилия </label>
                            <input className={'input'} type="text"
                                   name='lastName'
                                   onChange={(e) => {
                                       handleChange(e);
                                       onElementChange(e)
                                   }}
                                   onBlur={handleBlur}
                                   value={values.lastName}/>
                        </p>
                        {touched.lastName && errors.lastName && <p className='error'>{errors.lastName}</p>}

                        <p>
                            <label htmlFor="age">Возраст </label>
                            <input className={'input'} type="text"
                                   name='age'
                                   onChange={(e) => {
                                       handleChange(e);
                                       onElementChange(e)
                                   }}
                                   onBlur={handleBlur}
                                   value={values.age}/>
                        </p>
                        {touched.age && errors.age && <p className='error'>{errors.age}</p>}

                        <p>
                            <label htmlFor="address">Ваш адрес </label>
                            <input className={'input'} type="text"
                                   name='address'
                                   onChange={(e) => {
                                       handleChange(e);
                                       onElementChange(e)
                                   }}
                                   onBlur={handleBlur}
                                   value={values.address}/>
                        </p>
                        {touched.address && errors.address && <p className='error'>{errors.address}</p>}

                        <p>
                            <label htmlFor="phoneNumber">Телефон </label>
                            <input className={'input'} type="text"
                                   name='phoneNumber'
                                   onChange={(e) => {
                                       handleChange(e);
                                       onElementChange(e)
                                   }}
                                   onBlur={handleBlur}
                                   value={values.phoneNumber}/>
                        </p>
                        {touched.phoneNumber && errors.phoneNumber && <p className='error'>{errors.phoneNumber}</p>}
                        <Button
                                className='submit-btn'
                                disabled={!isValid && !dirty}
                                onClick={handleSubmit}
                                type='submit'
                                text='Купить'
                        />
                    </div>}
            </Formik>
        </div>
    )

}