import {GET_PRODUCTS} from '../actions/productActions'

const initialState = {
    data: []
};

export default function productsReducer(state = initialState, action) {
    switch (action.type) {
        case GET_PRODUCTS:
            return {
                data: action.dataProducts
            };
        default:
            return state
    }
}
