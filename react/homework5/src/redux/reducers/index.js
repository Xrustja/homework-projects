import productsReducer from "./productsReducer";
import {combineReducers} from "redux";
import modalReducer from "./modalReducer";
import formReducer from "./formReducer";
export const rootReducer = combineReducers({
    product: productsReducer,
    modal: modalReducer,
    form:formReducer
});