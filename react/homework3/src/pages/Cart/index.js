import React, {useEffect, useState} from 'react';
import ProductList from "../../components/ProductList";
import * as LocalStorageServices from "../../services/LocalStorageServices";

export default function Cart() {

    const [productsInCart, setProductsInCart] = useState([]);

    function getProducts() {
        fetch('products.json')
            .then(response => response.json())
            .then(data => {
                    const productsInLsCart = LocalStorageServices.getLSData('cart') || [];
                    setProductsInCart(productsInLsCart.map(id => data.find(product => product.id === id)).filter(item => item !== undefined));
                }
            )
    }

    useEffect(() => getProducts(), []);

    const pageTitle = productsInCart.length > 0 ? "Товары в вашей корзине:" : 'У вас нет товаров в корзине';
    return (
        <div>
            <h2>{pageTitle}</h2>
            <ProductList data={productsInCart} refresh={getProducts}/>
        </div>
    )
}

