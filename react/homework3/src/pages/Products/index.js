import React, {useEffect, useState} from 'react';
import ProductList from '../../components/ProductList'

export default function Products() {

    const [products, setProducts] = useState([]);

    function getProducts() {
        fetch('products.json')
            .then(response => response.json())
            .then(data => setProducts(data)
            )
    }

    useEffect(() => getProducts(), []);

    return (
        <div className="main">
            <ProductList data={products}/>
        </div>

    )
}








