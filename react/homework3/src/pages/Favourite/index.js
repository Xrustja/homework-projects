import React, {useEffect, useState} from 'react';
import ProductList from "../../components/ProductList";
import * as LocalStorageServices from "../../services/LocalStorageServices";

export default function Cart() {

    const [productsInFavourite, setProductsInFavourite] = useState([]);

    function getProducts() {
        fetch('products.json')
            .then(response => response.json())
            .then(data => {
                    const productsInLsFavourite = LocalStorageServices.getLSData('favourite') || [];
                    setProductsInFavourite(productsInLsFavourite.map(id => data.find(product => product.id === id)).filter(item => item !== undefined));

                }
            )
    }

    useEffect(() => getProducts(), []);

    const pageTitle = productsInFavourite.length > 0 ? "Ваши избранные продукты:" : 'У вас нет избранных товаров';

    return (
        <div>
            <h2>{pageTitle}</h2>
            <ProductList data={productsInFavourite} refresh={getProducts} />
        </div>
    )
}
