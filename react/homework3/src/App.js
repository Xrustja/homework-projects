import React from 'react';
import './App.scss';
import Navigation from './components/Navigation'
import RouterConfig from "./components/RouterConfig";
import {BrowserRouter as Router} from "react-router-dom";

function App() {
    return (
        <div className="App">
            <Router>
                <Navigation/>
                <RouterConfig/>
            </Router>
        </div>

    )
}


export default App;





