import React, {useEffect, useState} from 'react'
import PropTypes from 'prop-types'
import Button from '../Button'
import * as LocalStorageServices from "../../services/LocalStorageServices";
import Modal from "../Modal";
import './ProductCard.scss'


export default function ProductCard(props) {
    const [cart, setCart] = useState(false);
    const [favourite, setFavourite] = useState(false);
    const [modalStatus, setModalStatus] = useState(false);
    const [lastAction, setLastAction] = useState(null);

    function openModal() {
        setModalStatus(true)
    }

    function closeModal() {
        setModalStatus(false)
    }

    function addToFavourite(props) {
        let favouriteInLS = LocalStorageServices.getLSData('favourite') || [];
        if (!favourite) {
            favouriteInLS.push(props.id);
        } else {
            favouriteInLS = favouriteInLS.filter(LsId => LsId !== props.id);
            if (typeof (props.refresh) === 'function') props.refresh();
        }
        setFavourite(!favourite);
        LocalStorageServices.setLSData('favourite', favouriteInLS);
    }

    function handleClick() {
        if (!cart) {
            setCart(true);
            const cartInLS = LocalStorageServices.getLSData('cart') || [];
            cartInLS.push(props.id);
            LocalStorageServices.setLSData('cart', cartInLS);
            setLastAction('add');
        } else {
            setLastAction('delete');
        }
        openModal();
    }

    function removeFromCart(props) {
        if (cart) {
            const cartInLS = LocalStorageServices.getLSData('cart').filter(LsId => LsId !== props.id);
            if (typeof (cartInLS) === 'object') {
                LocalStorageServices.setLSData('cart', cartInLS);
                closeModal();
                setCart(false);
                if (typeof (props.refresh) === 'function') props.refresh();
            }
        }
    }

    useEffect(() => {
        if (LocalStorageServices.getLSData('favourite') !== null) {
            const favouriteInLS = LocalStorageServices.getLSData('favourite');
            if (favouriteInLS.includes(props.id)) {
                setFavourite(true)
            }
        }

        if (LocalStorageServices.getLSData('cart') !== null) {
            const favouriteInLS = LocalStorageServices.getLSData('cart');
            if (favouriteInLS.includes(props.id)) {
                setCart(true)
            }
        }


    }, []);


    const liked = favourite ? <i className="fas fa-heart"/> : <i className="far fa-heart"/>;
    const buyBtnText = cart ? 'Удалить с корзины' : 'Купить';

    return (
        <>

            <div className='card-container'>
                <img className='card-img' src={props.imgUrl} alt="Temporarily missing"/>
                <div className='card-text_container'>
                    <h4 className='card-name'>{props.name}</h4>
                    <h4 className='card-id'>Код товара: {props.id}</h4>
                </div>
                <div className='card-footer'>
                    <p className='card-price'>{props.price} ₴</p>
                    <Button
                        className='btn-favourite'
                        text={liked}
                        onClick={() => addToFavourite(props)}
                    />
                    <Button
                        productName={props.name}
                        className='card-button'
                        text={buyBtnText}
                        onClick={() => handleClick()}/>
                </div>
            </div>
            {(lastAction === 'add')
                ?
                <Modal
                    closeButton={true}
                    text={`${props.name} добавлен в корзину`}
                    onClose={closeModal}
                    show={modalStatus}
                    actions={[
                        <Button
                            key='1'
                            className='btn-modal'
                            text='Оформить заказ'
                            onClick={() => alert('Отличный выбор!')}
                        />
                    ]}
                />
                :
                <Modal
                    closeButton={true}
                    title={'Подтвердите удаление'}
                    text={`Вы действительно хотите удалить ${props.name} с корзины?`}
                    onClose={closeModal}
                    show={modalStatus}
                    actions={[
                        <Button
                            key='1'
                            className='btn-modal'
                            text='Отменить'
                            onClick={closeModal}
                        />,
                        <Button
                            key='2'
                            className='btn-modal'
                            text='Удалить'
                            onClick={() => removeFromCart(props)}
                        />
                    ]}
                />}
        </>
    )

}

ProductCard.propTypes = {
    price: PropTypes.string,
    id: PropTypes.string.isRequired,
    imgUrl: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    onClick: PropTypes.func,
};

ProductCard.defaultProps = {
    price: 'Нет в наличии'
};

