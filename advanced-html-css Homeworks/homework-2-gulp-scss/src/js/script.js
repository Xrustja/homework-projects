$(document).ready(function () {
    $('.nav__btn').on('click', function () {
        $('.nav__list').toggleClass('hidden');
        $('#nav__btn').toggleClass('fa-bars').toggleClass('fa-times');
    });
});
