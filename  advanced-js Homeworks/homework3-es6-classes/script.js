class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }
}

class Programmer extends Employee{

    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get salary() {
        return this._salary*3;
    }
}

console.log(new Programmer('Pasha', 25, 500, ['Java', 'C++']));
console.log(new Programmer('Christina', 30, 1000, ['JavaScript', 'HTML']));
console.log(new Programmer('Segrey', 30, 2500, ['Java', 'Javascript']));