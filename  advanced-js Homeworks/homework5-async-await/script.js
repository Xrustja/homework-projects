async function getAddress() {
    let div = document.createElement('div');
    let btn = document.getElementById('btn');
    div.innerHTML = `We are looking for you...`;
    btn.after(div);

    let ipResponse = await fetch('https://api.ipify.org/?format=json');
    let jsonIpResp = await ipResponse.json();
    let ip = jsonIpResp.ip;

    let addressResponse = await fetch(`http://ip-api.com/json/${ip}`);
    let addressJson = await addressResponse.json();
    let {timezone, country, regionName, city, zip} = addressJson;
    setTimeout(() => {
        div.innerHTML = `<strong>You are from:</strong>  ${timezone},  ${country},  ${regionName},  ${city},  ${zip}`
    }, 2000)
}

