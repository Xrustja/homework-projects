document.addEventListener('DOMContentLoaded', function () {

    const books = [
        {
            author: "Скотт Бэккер",
            name: "Тьма, что приходит прежде",
            price: 70
        },
        {
            author: "Скотт Бэккер",
            name: "Воин-пророк",
        },
        {
            name: "Тысячекратная мысль",
            price: 70
        },
        {
            author: "Скотт Бэккер",
            name: "Нечестивый Консульт",
            price: 70
        },
        {
            author: "Дарья Донцова",
            name: "Детектив на диете",
            price: 40
        },
        {
            author: "Дарья Донцова",
            name: "Дед Снегур и Морозочка",
        }
    ];
    let book = {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    };

    let rootEl = document.getElementById('root');

    function buildBook(book) {
        return `<li> Book: <ul> ${Object.entries(book).map(([key, value]) =>
            `<li> ${key}: ${value}</li>`
        ).join('')}</ul></li>`
    }

    function validateBook(book) {
        if (!book.author) {
            throw new Error(`Author is not specified in book parameters`)
        }
        if (!book.name) {
            throw new Error(`Name is not specified in book parameters`);
        }
        if (!book.price) {
            throw new Error(`Price is not specified in book parameters`);
        }

        return buildBook(book);
    }

    function buildList(books) {
        return rootEl.insertAdjacentHTML("beforeend", (`<ul> ${books.map(
            book => {
                try {
                    return validateBook(book)
                } catch (e) {
                    console.log(e.message);
                }
            }).join('')}</ul>`))
    }

    buildList(books);

});
