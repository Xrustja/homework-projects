class Film {
    constructor({title, episode_id, opening_crawl, characters}) {
        this.title = title;
        this.episode_id = episode_id;
        this.opening_crawl = opening_crawl;
        this.characters = characters;
    }

    addToDOM(ul) {
        let li = document.createElement('li');
        li.innerHTML = 'Film:';
        ul.append(li);
        for (let key in this) {
            let ulForFilm = document.createElement('ul');
            let liForFilm = document.createElement('li');
            let value = this[key];
            if (key !== 'characters') {
                liForFilm.innerHTML = `<strong>${key}: </strong>${value}`;
            } else {
                liForFilm.innerHTML = `<strong>characters: </strong>`;
                let ulForCharacter = document.createElement('ul');
                ulForCharacter.innerHTML = `<em> Loading...</em>`;
                liForFilm.append(ulForCharacter);
                Promise.all(value.map(characters =>
                    fetch(characters)
                        .then(result => result.json())
                        .then(result => {
                            let liForCharacter = document.createElement('li');
                            liForCharacter.innerHTML = `${result.name}`;
                            return liForCharacter
                        })
                ))
                    .then(result => {
                        ulForCharacter.innerHTML = '';
                        result.forEach(charName => ulForCharacter.append(charName))
                    });
            }
            li.append(ulForFilm);
            ulForFilm.append(liForFilm);
        }

    }
}

document.addEventListener('DOMContentLoaded', function () {
    let ul = document.createElement('ul');
    ul.innerHTML = 'Loading...';
    document.body.append(ul);

    function getFilm(url) {
        ul.innerHTML = '';
        fetch(url)
            .then(result => result.json())
            .then(result => {
                result.results
                    .map(film => new Film(film))
                    .forEach(elem =>
                        elem.addToDOM(ul)
                    )
            });
    }

    getFilm('https://swapi.dev/api/films/');
});